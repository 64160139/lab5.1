package com.fah.week5;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class SelectionsortAppTest {
    @Test
    public void shouldFineMinIndexTestcase1(){
        int [] arr = {5,4,3,2,1};
        int pos = 0;
        int min = SelectionsortApp.fineMinIndex(arr,pos);
        assertEquals(4,min);
    }
    @Test
    public void shouldFineMinIndexTestcase2(){
        int [] arr = {1,4,3,2,5};
        int pos = 1;
        int min = SelectionsortApp.fineMinIndex(arr,pos);
        assertEquals(3,min);
    }
    @Test
    public void shouldFineMinIndexTestcase3(){
        int [] arr = {1,2,3,4,5};
        int pos = 2;
        int min = SelectionsortApp.fineMinIndex(arr,pos);
        assertEquals(2,min);
    }
    
}
